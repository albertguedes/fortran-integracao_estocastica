    PROGRAM integracao_estocastica

    use library

    implicit none

    ! m series
    ! n amostras por série
    integer m,n
    parameter(m=100000)
    parameter(n=100000)

    ! a intervalo mínimo a ser avaliado.
    ! b intervalo maximo a ser avaliado.
    ! max é o teto da caixa que se encontra a função.
    ! area é a área total do quadrado.
    real a,b, y_m, area
    parameter( a = 0.0 )
    parameter( b = 1.0 )
    parameter( y_m = 2.718281828 )
    parameter( area = ( b - a )*y_m )

    integer i,j

    ! f é a frequencia em que um ponto cai dentro da função.
    ! media é a média de frequências.
    ! erro é o erro das frequencias.
    real f,media,erro

    ! x,y são as coordenadas de um ponto aleatório do quadrado.
    real x, y

    do i=1,m

        f = 0
        do j=1, n

            x = a + ( b - a )*random()
            y = y_m*random()

            if( y < phi(x) )then
                f = f + 1
            endif

        enddo
        f = f/n

        media = media + f
        erro  = erro + f**2

    enddo

    ! <f> = ( sum f_i ) / m
    media = media/m

    ! sig^2(f) = <f^2> - <f>^2
    erro = sqrt( erro/m - media**2 )

    write(*,"(F10.8,A,F10.8)"), area*media," +/- ", area*erro

    END PROGRAM integracao_estocastica
